<?php
declare(strict_types=1);

namespace Listing\Controller;

use App\Controller\ApiController;
use Listing\Model\Table\NotebooksTable;

/**
 * @property NotebooksTable $Notebooks
 */
class NotebooksPublicListController extends ApiController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->Notebooks = NotebooksTable::load();
    }

    public function isPublicController(): bool
    {
        return true;
    }

    protected function getData($public_key)
    {
        $notebook = $this->Notebooks->findPublicListByKey($public_key)->firstOrFail();
        $this->return = $notebook;
    }
}
