<?php
declare(strict_types=1);

namespace Listing\Controller;

use App\Controller\ApiController;
use Cake\ORM\Query;
use Listing\Model\Entity\Notebook;
use Listing\Model\Table\NotebooksTable;

/**
 * @property NotebooksTable $Notebooks
 */
class NotebooksController extends ApiController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->Notebooks = NotebooksTable::load();
    }

    public function isPublicController(): bool
    {
        return false;
    }

    private function _urlCondition($urlFilter): \Closure
    {
        return function (Query $q) use ($urlFilter) {
            return $q->where(['url' => $urlFilter]);
        };
    }

    protected function addNew($data)
    {
        $notebook = $this->Notebooks->newEmptyEntity();
        /** @var Notebook $notebook */
        $notebook = $this->Notebooks->patchEntity($notebook, $data);

        $notebook->user_id = $this->request->getParam('userID');

        $saved = $this->Notebooks->saveOrFail($notebook);

        $this->return = $this->Notebooks->get($saved->id);
    }

    protected function getData($notebookId)
    {
        $userId = $this->request->getParam('userID');
        $notebook = $this->Notebooks->findNotebookByIdAndUser($notebookId, $userId)->firstOrFail();
        $this->return = $notebook;
    }

    protected function getList()
    {
        $urlFilter = $this->request->getQuery('url');
        $userId = $this->request->getParam('userID');
        $query = $this->Notebooks->findNotebooksByUser($userId)->orderDesc('Notebooks.id');
        if ($urlFilter) {
            $query
                ->contain('Notes', $this->_urlCondition($urlFilter))
                ->innerJoinWith('Notes', $this->_urlCondition($urlFilter));
        }
        $this->return = $query->toArray();
    }

    protected function edit($id, $data)
    {
        $userId = $this->request->getParam('userID');

        $notebook = $this->Notebooks->findNotebookByIdAndUser($id, $userId)->firstOrFail();
        $notebook = $this->Notebooks->patchEntity($notebook, $data);


        $saved = $this->Notebooks->saveOrFail($notebook);
        $this->return = $this->Notebooks->get($saved->id);
    }

    public function delete($id)
    {
        $userId = $this->request->getParam('userID');
        $this->Notebooks->findNotebookByIdAndUser($id, $userId)->firstOrFail();

        $this->Notebooks->softDelete($id);
        $this->return = false;
    }
}
