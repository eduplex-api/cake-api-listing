<?php
declare(strict_types=1);

namespace Listing\Controller;

use App\Controller\ApiController;
use Listing\Model\Entity\Note;
use Listing\Model\Table\NotesTable;
use Listing\Model\Table\NotebooksTable;
use Cake\Http\Exception\ForbiddenException;

/**
 * @property NotesTable $Notes
 * @property NotebooksTable $Notebooks
 */
class NotesController extends ApiController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->Notebooks = NotebooksTable::load();
        $this->Notes = NotesTable::load();
    }

    public function isPublicController(): bool
    {
        return false;
    }

    protected function addNew($data)
    {
        $this->_checkNotebookAccess();
        $note = $this->Notes->newEmptyEntity();
        /** @var Note $note */
        $note = $this->Notes->patchEntity($note, $data);

        $note->notebook_id = $this->request->getParam('notebook_id');

        $saved = $this->Notes->saveOrFail($note);

        $this->return = $this->Notes->get($saved->id);
    }

    protected function getList()
    {
        $this->_checkNotebookAccess();
        $notebookId = $this->request->getParam('notebook_id');
        $notes = $this->Notes->findNotesByNotebook($notebookId)->toArray();
        $this->return = $notes;
    }

    protected function getData($id)
    {
        /** @var Note $note */
        $this->_checkNotebookAccess();
        $note = $this->Notes->getNoteById($id);

        $this->return = $note;
    }

    protected function edit($id, $data)
    {
        $this->_checkNotebookAccess();
        $note = $this->Notes->get($id);
        $note = $this->Notes->patchEntity($note, $data);

        $saved = $this->Notes->saveOrFail($note);
        $this->return = $this->Notes->get($saved->id);
    }

    public function delete($id)
    {
        $this->_checkNotebookAccess();
        $this->Notes->get($id);
        $this->Notes->softDelete($id);
        $this->return = false;
    }

    private function _checkNotebookAccess(): void
    {
        $notebookId = $this->request->getParam('notebook_id');
        $userId = $this->request->getParam('userID');
        $notebook = $this->Notebooks->findNotebookByIdAndUser($notebookId, $userId)
            ->first();
        if (!$notebook) {
            throw new ForbiddenException('UserID does not match notebookID');
        }
    }
}
