<?php
declare(strict_types=1);

namespace Listing\Model\Entity;

use Cake\ORM\Entity;

/**
 * @property string $title
 * @property mixed $user_id
 * @property string $shape
 * @property string $icon
 * @property string $public_key
 * @property mixed $created
 * @property mixed $modified
 */
class Notebook extends Entity
{
    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
    }

    protected $_accessible = [
        '*' => false,
        'id' => false,
        'user_id' => false,

        'icon' => true,
        'title' => true,
        'shape' => true,
        'public_key' => true,
        'created' => true,
        'modified' => true,
    ];

    protected $_hidden = [
        'deleted'
    ];

    protected $_virtual = [
    ];
}
