<?php
declare(strict_types=1);

namespace Listing\Model\Entity;

use Cake\ORM\Entity;

/**
 * @property mixed $notebook_id
 * @property string $title
 * @property mixed $date
 * @property string $description
 * @property mixed $completed
 * @property string $image
 * @property string $url
 * @property integer $position
 */
class Note extends Entity
{

    protected $_accessible = [
        '*' => false,
        'id' => false,

        'title' => true,
        'date' => true,
        'description' => true,
        'completed' => true,
        'image' => true,
        'url' => true,
        'position' => true,
    ];

    protected $_hidden = [
        'deleted'
    ];

}
