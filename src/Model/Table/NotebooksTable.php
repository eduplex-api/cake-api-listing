<?php
declare(strict_types=1);

namespace Listing\Model\Table;

use App\Model\Table\UsersTable;
use Cake\ORM\Behavior\TimestampBehavior;
use Listing\Lib\Consts\NotebookShapes;
use Cake\ORM\Query;
use Cake\Validation\Validator;
use Listing\Model\Entity\Notebook;
use RestApi\Lib\Validator\RestApiValidator;
use RestApi\Model\Table\RestApiTable;

/**
 * @property NotesTable $Notes
 */
class NotebooksTable extends RestApiTable
{
    public static function load(): self
    {
        /** @var NotebooksTable $table */
        $table = parent::load();
        return $table;
    }

    public function initialize(array $config): void
    {
        $this->addBehavior(TimestampBehavior::class);
        UsersTable::addHasMany($this)->setForeignKey('user_id');
        NotesTable::addBelongsTo($this)->setForeignKey('notebook_id');
    }

    public function copyAsOwn($notebookId, $userId)
    {
        /** @var Notebook $notebook */
        $notebook = $this->get($notebookId);
        $notebook->id = null;
        $notebook->user_id = $userId;
        $notebook->public_key = null;
        $notebook->setNew(true);
        $newNotebook = $this->saveOrFail($notebook);

        $notes = $this->Notes->find()->where(['notebook_id' => $notebookId])->all();
        foreach ($notes as $note) {
            $note->id = null;
            $note->notebook_id = $newNotebook->id;
            $note->setNew(true);
        }
        $this->Notes->saveManyOrFail($notes);
    }

    public function findNotebooksByUser($userId) : Query
    {
        return $this->find()
            ->where(['user_id' => $userId]);
    }

    public function findNotebookByIdAndUser($id, $userId) : Query
    {
        $notebook = $this->find()
            ->where(['id' => $id, 'user_id' => $userId]);

        return $notebook;
    }

    public function findPublicListByKey($key) : Query
    {
        return $this->find()
            ->where(['public_key' => $key])
            ->contain([
                'Notes' => [
                    'sort' => ['position' => 'desc']
                ]
            ]);
    }

    public function validationDefault(Validator $validator): Validator
    {
        /** @var RestApiValidator $validator */
        $shapes = [
            NotebookShapes::TODO,
            NotebookShapes::LIST,
            NotebookShapes::NOTES,
            NotebookShapes::IMPROVE_SKILLS,
            NotebookShapes::NEW_SKILLS
        ];

        return $validator
            ->inList('shape', $shapes);
    }
}
