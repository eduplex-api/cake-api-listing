<?php
declare(strict_types=1);

namespace Listing\Model\Table;

use Cake\ORM\Behavior\TimestampBehavior;
use Listing\Model\Entity\Note;
use Cake\ORM\Query;
use RestApi\Model\Table\RestApiTable;

class NotesTable extends RestApiTable
{
    public static function load(): self
    {
        /** @var NotesTable $note */
        $note = parent::load();
        return $note;
    }

    public function initialize(array $config): void
    {
        $this->addBehavior(TimestampBehavior::class);
        NotebooksTable::addHasMany($this)->setForeignKey('notebook_id');
    }

    public function findNotesByNotebook($notebookId) : Query
    {
        return $this->find()
            ->where(['notebook_id' => $notebookId])
            ->order(['position' => 'desc']);
    }

    public function getNoteById($id) : Note
    {
        /** @var Note $note */
        $note = $this->find()
            ->where(['id' => $id])
            ->firstOrFail();

        return $note;
    }
}
