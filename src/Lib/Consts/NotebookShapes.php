<?php
declare(strict_types=1);

namespace Listing\Lib\Consts;

class NotebookShapes
{
    const TODO = 'todo';
    const LIST = 'list';
    const NOTES = 'notes';
    const IMPROVE_SKILLS = 'improve_sk';
    const NEW_SKILLS = 'new_sk';
}
