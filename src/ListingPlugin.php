<?php
declare(strict_types=1);

namespace Listing;

use Cake\Routing\RouteBuilder;
use Listing\Controller\NotesController;
use RestApi\Lib\RestPlugin;

class ListingPlugin extends RestPlugin
{
    protected function routeConnectors(RouteBuilder $builder): void
    {
        $builder->connect('/users/{userID}/notebooks/{notebook_id}/notes/*', NotesController::route());
        $builder->connect('/users/{userID}/notebooks/*', \Listing\Controller\NotebooksController::route());
        $builder->connect('/notebooks/*', \Listing\Controller\NotebooksPublicListController::route());
        $builder->connect('/listing/openapi/*', \Listing\Controller\SwaggerJsonController::route());
    }
}
