# Cake-API-Listing

Simple API to manage **lists** of learning resources, playlists, notes, etc.

## Works with
CakePHP Plugin to run on top of [cake-rest-api](https://packagist.org/packages/freefri/cake-rest-api).

## Openapi documentation

Swagger UI in [/edu/api/v1/listing/openapi/](https://proto.eduplex.eu/edu/api/v1/listing/openapi/)

## License
The source code for the site is licensed under the [**MIT license**](https://gitlab.com/eduplex-api), which you can find in the [LICENSE](../LICENSE/) file.
