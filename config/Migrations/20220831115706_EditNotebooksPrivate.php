<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class EditNotebooksPrivate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table(\Listing\ListingPlugin::getTablePrefix() . 'notebooks');
        $table->removeColumn('private');
        $table->addColumn('public_key', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => true,
        ]);
        $table->addIndex(['public_key']);
        $table->addIndex(['user_id']);
        $table->update();
    }
}
