<?php
declare(strict_types=1);

namespace Listing\Test\Fixture;

use App\Test\Fixture\UsersFixture;
use Listing\Lib\Consts\NotebookShapes;
use RestApi\TestSuite\Fixture\RestApiFixture;

class NotebooksFixture extends RestApiFixture
{
    const LOAD = 'plugin.Listing.Notebooks';

    public $records = [
        [
            'id' => 1,
            'user_id' => UsersFixture::BUYER_ID,
            'title' => 'Title 1',
            'shape' => NotebookShapes::TODO,
            'icon' => 'mdi-check-circle',
            'created' => '2021-01-18 10:39:23',
            'modified' => '2021-01-18 10:41:31',
            'public_key' => null
        ],
        [
            'id' => 2,
            'user_id' => UsersFixture::BUYER_ID,
            'title' => 'Private list',
            'shape' => NotebookShapes::LIST,
            'icon' => 'mdi-clipboard-list-outline',
            'created' => '2021-01-18 10:39:23',
            'modified' => '2021-01-18 10:41:31',
            'public_key' => '1b6a83a918438b'
        ],
        [
            'id' => 3,
            'user_id' => UsersFixture::BUYER_ID,
            'title' => 'Public list',
            'shape' => NotebookShapes::LIST,
            'icon' => 'mdi-clipboard-list-outline',
            'created' => '2021-01-18 10:39:23',
            'modified' => '2021-01-18 10:41:31',
            'public_key' => null
        ]
    ];
}
