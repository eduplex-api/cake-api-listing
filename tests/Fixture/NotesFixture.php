<?php
declare(strict_types=1);

namespace Listing\Test\Fixture;

use RestApi\TestSuite\Fixture\RestApiFixture;

class NotesFixture extends RestApiFixture
{
    const LOAD = 'plugin.Listing.Notes';

    public $records = [
        [
            'id' => 1,
            'notebook_id' => 1,
            'title' => 'Note 1',
            'date' => '2021-01-20 19:39:23',
            'description' => 'Note test 1',
            'completed' => '2021-01-25 16:39:23',
            'position' => 0,
            'created' => '2021-01-18 10:39:23',
            'modified' => '2021-01-18 10:41:31',
            'image' => null,
            'url' => null,
        ],
        [
            'id' => 2,
            'notebook_id' => 2,
            'title' => 'Note from notebook 2',
            'date' => '2021-01-20 19:39:23',
            'description' => 'Note test 2',
            'completed' => '2021-01-25 16:39:23',
            'position' => 1,
            'created' => '2021-01-18 10:39:23',
            'modified' => '2021-01-18 10:41:31',
            'image' => null,
            'url' => null,
        ],
        [
            'id' => 3,
            'notebook_id' => 3,
            'title' => 'Event 1 from Wish list 1',
            'date' => '2021-01-20 19:39:23',
            'description' => null,
            'completed' => null,
            'position' => 2,
            'created' => '2021-01-18 10:39:23',
            'modified' => '2021-01-18 10:41:31',
            'image' => 'https://img.rawpixel.com/private/static/images/website/2022-05/fl12929410165-image-kybeoc61.jpg?w=800&dpr=1&fit=default&crop=default&q=65&vib=3&con=3&usm=15&bg=F4F4F3&ixlib=js-2.2.1&s=8d6e7f2dee775c830a74c4e0af9f0460',
            'url' => 'https://www.courseticket.com/en/e/50',
        ],
        [
            'id' => 4,
            'notebook_id' => 2,
            'title' => 'Event 1 from Wish list 1',
            'date' => '2021-01-20 19:39:23',
            'description' => null,
            'completed' => null,
            'position' => 2,
            'created' => '2021-01-18 10:39:23',
            'modified' => '2021-01-18 10:41:31',
            'image' => 'https://img.rawpixel.com/private/static/images/website/2022-05/fl12929410165-image-kybeoc61.jpg?w=800&dpr=1&fit=default&crop=default&q=65&vib=3&con=3&usm=15&bg=F4F4F3&ixlib=js-2.2.1&s=8d6e7f2dee775c830a74c4e0af9f0460',
            'url' => 'https://www.courseticket.com/en/e/50',
        ],
        [
            'id' => 5,
            'notebook_id' => 3,
            'title' => 'Module 1 from Wish list 1',
            'date' => '2021-01-20 19:39:23',
            'description' => null,
            'completed' => null,
            'position' => 2,
            'created' => '2021-01-18 10:39:23',
            'modified' => '2021-01-18 10:41:31',
            'image' => 'https://img.rawpixel.com/private/static/images/website/2022-05/fl12929410165-image-kybeoc61.jpg?w=800&dpr=1&fit=default&crop=default&q=65&vib=3&con=3&usm=15&bg=F4F4F3&ixlib=js-2.2.1&s=8d6e7f2dee775c830a74c4e0af9f0460',
            'url' => 'https://www.courseticket.com/en/e/50?url=24',
        ]
    ];
}
