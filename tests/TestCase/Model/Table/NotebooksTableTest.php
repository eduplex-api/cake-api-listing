<?php
declare(strict_types=1);

namespace Listing\Test\TestCase\Model\Table;

use Cake\TestSuite\TestCase;
use Listing\Model\Table\NotebooksTable;
use Listing\Test\Fixture\NotebooksFixture;
use Listing\Test\Fixture\NotesFixture;

class NotebooksTableTest extends TestCase
{
    protected $fixtures = [
        NotebooksFixture::LOAD,
        NotesFixture::LOAD,
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->Notebooks = NotebooksTable::load();
    }

    public function testCopyAsOwn()
    {
        $uid = 88;
        $this->Notebooks->copyAsOwn(1, $uid);

        $notebooks = $this->Notebooks->find()->orderDesc('id')->all();
        $dbNotebook = $notebooks->first();
        $this->assertEquals('Title 1', $dbNotebook->title);
        $this->assertEquals($uid, $dbNotebook->user_id);
        $this->assertEquals(null, $dbNotebook->public_key);
        $this->assertEquals(4, $notebooks->count());
        $notes = $this->Notebooks->Notes->find()->where(['notebook_id' => $dbNotebook->id])->all();
        $this->assertEquals(1, $notes->count());
    }
}
