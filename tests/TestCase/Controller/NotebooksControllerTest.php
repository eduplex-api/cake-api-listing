<?php
declare(strict_types=1);

namespace Listing\Test\TestCase\Controller;

use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use Listing\Lib\Consts\NotebookShapes;
use Listing\ListingPlugin;
use Listing\Model\Table\NotebooksTable;
use Listing\Test\Fixture\NotebooksFixture;
use Listing\Test\Fixture\NotesFixture;
use RestApi\TestSuite\ApiCommonErrorsTest;

class NotebooksControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        NotebooksFixture::LOAD,
        NotesFixture::LOAD,
        UsersFixture::LOAD,
        OauthAccessTokensFixture::LOAD
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
    }

    protected function _getEndpoint(): string
    {
        return ListingPlugin::getRoutePath() . '/users/' . UsersFixture::BUYER_ID . '/notebooks/';
    }

    public function testAddNew()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'title' => 'Title 1',
            'shape' => NotebookShapes::TODO,
            'icon' => 'mdi-check-circle',
        ];

        $expected = [
            'id' => 4,
            'user_id' => UsersFixture::BUYER_ID,
            'title' => 'Title 1',
            'shape' => NotebookShapes::TODO,
            'icon' => 'mdi-check-circle',
            'public_key' => null
        ];

        $this->post($this->_getEndpoint(), $data);
        $this->assertJsonResponseOK();

        $return = json_decode($this->_getBodyAsString(), true)['data'];
        unset($return['created']);
        unset($return['modified']);
        $this->assertEquals($expected, $return);
    }

    public function testAddNew_PrivateList()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'title' => 'List A',
            'shape' => NotebookShapes::LIST,
            'icon' => 'mdi-clipboard-list-outline',
            'public_key' => '1b6a83a918438b'
        ];

        $expected = [
            'user_id' => UsersFixture::BUYER_ID,
            'title' => 'List A',
            'shape' => NotebookShapes::LIST,
            'icon' => 'mdi-clipboard-list-outline',
            'public_key' => '1b6a83a918438b'
        ];

        $this->post($this->_getEndpoint(), $data);
        $this->assertJsonResponseOK();

        $return = json_decode($this->_getBodyAsString(), true)['data'];
        unset($return['created']);
        unset($return['modified']);
        $this->assertEqualsNoId($expected, $return);
    }

    public function testGetNotebook_ById()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $expectedData = [
            'id' => 1,
            'user_id' => UsersFixture::BUYER_ID,
            'title' => 'Title 1',
            'shape' => NotebookShapes::TODO,
            'icon' => 'mdi-check-circle',
            'created' => '2021-01-18T10:39:23+01:00',
            'modified' => '2021-01-18T10:41:31+01:00',
            'public_key' => null
        ];

        $this->get($this->_getEndpoint().'1');
        $return = $this->assertJsonResponseOK()['data'];
        $this->assertEquals($expectedData, $return);
    }

    public function testGetList_shouldReturnFilteredNotes()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $expectedData = [
            [
                'id' => 3,
                'user_id' => UsersFixture::BUYER_ID,
                'title' => 'Public list',
                'shape' => NotebookShapes::LIST,
                'icon' => 'mdi-clipboard-list-outline',
                'created' => '2021-01-18T10:39:23+01:00',
                'modified' => '2021-01-18T10:41:31+01:00',
                'public_key' => null,
                'notes' => [
                    [
                        'id' => 3,
                        'notebook_id' => 3,
                        'title' => 'Event 1 from Wish list 1',
                        'date' => '2021-01-20T19:39:23+01:00',
                        'description' => null,
                        'completed' => null,
                        'image' => 'https://img.rawpixel.com/private/static/images/website/2022-05/fl12929410165-image-kybeoc61.jpg?w=800&dpr=1&fit=default&crop=default&q=65&vib=3&con=3&usm=15&bg=F4F4F3&ixlib=js-2.2.1&s=8d6e7f2dee775c830a74c4e0af9f0460',
                        'position' => 2,
                        'created' => '2021-01-18T10:39:23+01:00',
                        'modified' => '2021-01-18T10:41:31+01:00',
                        'url' => 'https://www.courseticket.com/en/e/50'
                    ]
                ],
            ],
            [
                'id' => 2,
                'user_id' => UsersFixture::BUYER_ID,
                'title' => 'Private list',
                'shape' => NotebookShapes::LIST,
                'icon' => 'mdi-clipboard-list-outline',
                'created' => '2021-01-18T10:39:23+01:00',
                'modified' => '2021-01-18T10:41:31+01:00',
                'public_key' => '1b6a83a918438b',
                'notes' => [
                    [
                        'id' => 4,
                        'notebook_id' => 2,
                        'title' => 'Event 1 from Wish list 1',
                        'date' => '2021-01-20T19:39:23+01:00',
                        'description' => null,
                        'completed' => null,
                        'image' => 'https://img.rawpixel.com/private/static/images/website/2022-05/fl12929410165-image-kybeoc61.jpg?w=800&dpr=1&fit=default&crop=default&q=65&vib=3&con=3&usm=15&bg=F4F4F3&ixlib=js-2.2.1&s=8d6e7f2dee775c830a74c4e0af9f0460',
                        'position' => 2,
                        'created' => '2021-01-18T10:39:23+01:00',
                        'modified' => '2021-01-18T10:41:31+01:00',
                        'url' => 'https://www.courseticket.com/en/e/50'
                    ]
                ],
            ]
        ];

        $this->get($this->_getEndpoint().'?url=https://www.courseticket.com/en/e/50');
        $this->assertJsonResponseOK();
        $return = json_decode($this->_getBodyAsString(), true)['data'];
        $this->assertEquals($expectedData, $return);
    }

    public function testGetList_NonExistingNotes_shouldReturnFilteredNotes()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $expectedData = [];

        $this->get($this->_getEndpoint().'?url=https://www.test.com/en/e/50');
        $this->assertJsonResponseOK();
        $return = json_decode($this->_getBodyAsString(), true)['data'];
        $this->assertEquals($expectedData, $return);
    }

    public function testGetNotebook_IncorrectUserInUrl_returnsNotFound()
    {
        $this->get($this->_getEndpoint());
        $this->assertException('Forbidden', 403, 'Resource not allowed with this token');
    }

    public function testGetList_GetUser1_GetsSingleUser()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $expectedData = [
            [
                'id' => 3,
                'user_id' => UsersFixture::BUYER_ID,
                'title' => 'Public list',
                'shape' => NotebookShapes::LIST,
                'icon' => 'mdi-clipboard-list-outline',
                'created' => '2021-01-18T10:39:23+01:00',
                'modified' => '2021-01-18T10:41:31+01:00',
                'public_key' => null
            ],
            [
                'id' => 2,
                'user_id' => UsersFixture::BUYER_ID,
                'title' => 'Private list',
                'shape' => NotebookShapes::LIST,
                'icon' => 'mdi-clipboard-list-outline',
                'created' => '2021-01-18T10:39:23+01:00',
                'modified' => '2021-01-18T10:41:31+01:00',
                'public_key' => '1b6a83a918438b'
            ],
            [
                'id' => 1,
                'user_id' => UsersFixture::BUYER_ID,
                'title' => 'Title 1',
                'shape' => NotebookShapes::TODO,
                'icon' => 'mdi-check-circle',
                'created' => '2021-01-18T10:39:23+01:00',
                'modified' => '2021-01-18T10:41:31+01:00',
                'public_key' => null
            ],
        ];

        $this->get($this->_getEndpoint());

        $this->assertJsonResponseOK();
        $return = json_decode($this->_getBodyAsString(), true)['data'];
        $this->assertEquals($expectedData, $return);
    }

    public function testEdit_shouldChangeTitle()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'title' => 'Test'
        ];

        $this->patch($this->_getEndpoint() . '1', $data);
        $this->assertJsonResponseOK();
        $return = json_decode($this->_getBodyAsString(), true)['data'];
        $this->assertEquals($data['title'], $return['title']);
    }

    public function testEdit_changeToPrivateList()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'title' => 'Test',
            'public_key' => '1b6a83a918438b'
        ];
        $this->patch($this->_getEndpoint() . '1', $data);
        $this->assertJsonResponseOK();
        $return = json_decode($this->_getBodyAsString(), true)['data'];
        $this->assertEquals($data['title'], $return['title']);
        $this->assertEquals($data['public_key'], $return['public_key']);
    }

    public function testEdit_notebookFromOtherUser_NotFound()
    {
        $endpoint = ListingPlugin::getRoutePath() . '/users/' . UsersFixture::SELLER_ID . '/notebooks/1';
        $data = [
            'title' => 'Test'
        ];

        $this->patch($endpoint, $data);

        $this->assertResponseCode(404);
    }

    public function testEdit_EditInvalidShape_ThrowsValidationError()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'title' => 'Test',
            'shape' => 'something'
        ];

        $expectedErrorFields = [
            'shape' => [
                'inList' => 'The provided value is invalid'
            ]
        ];

        $this->patch($this->_getEndpoint() . '1', $data);
        $this->assertResponseError();
        $this->assertResponseCode(400);
        $return = json_decode($this->_getBodyAsString(), true);
        $this->assertEquals($expectedErrorFields, $return['error_fields']);
    }

    public function testDelete_notebook()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $userId = 1;
        $notebookId = 1;

        $this->delete($this->_getEndpoint() . $notebookId);
        $this->assertResponseOk($this->_getBodyAsString());

        $notebook = NotebooksTable::load()->findNotebookByIdAndUser($notebookId, $userId)
            ->first();

        $this->assertNull($notebook);
    }

    public function testDelete_NonExistingNotebook_NotFound()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $notebookId = 15;
        $this->delete($this->_getEndpoint() . $notebookId);
        $this->assertResponseError($this->_getBodyAsString());
    }

    public function testDelete_notebookFromOtherUser_NotFound()
    {
        $endpoint = ListingPlugin::getRoutePath() . '/users/' . UsersFixture::SELLER_ID . '/notebooks/1';

        $this->delete($endpoint);
        $this->assertResponseCode(404);
    }
}
