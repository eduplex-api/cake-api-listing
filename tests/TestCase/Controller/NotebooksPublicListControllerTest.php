<?php
declare(strict_types=1);

namespace Listing\Test\TestCase\Controller;

use App\Test\Fixture\UsersFixture;
use Listing\Lib\Consts\NotebookShapes;
use Listing\ListingPlugin;
use Listing\Test\Fixture\NotebooksFixture;
use Listing\Test\Fixture\NotesFixture;
use RestApi\TestSuite\ApiCommonErrorsTest;

class NotebooksPublicListControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        NotebooksFixture::LOAD,
        NotesFixture::LOAD,
    ];

    protected function _getEndpoint(): string
    {
        return ListingPlugin::getRoutePath() . '/notebooks/';
    }

    public function testGetList_fromPublicToken()
    {
        $expectedData = [
            'id' => 2,
            'user_id' => UsersFixture::BUYER_ID,
            'title' => 'Private list',
            'shape' => NotebookShapes::LIST,
            'icon' => 'mdi-clipboard-list-outline',
            'created' => '2021-01-18T10:39:23+01:00',
            'modified' => '2021-01-18T10:41:31+01:00',
            'public_key' => '1b6a83a918438b',
            'notes' => [
                [
                    'id' => 4,
                    'notebook_id' => 2,
                    'title' => 'Event 1 from Wish list 1',
                    'date' => '2021-01-20T19:39:23+01:00',
                    'description' => null,
                    'completed' => null,
                    'position' => 2,
                    'created' => '2021-01-18T10:39:23+01:00',
                    'modified' => '2021-01-18T10:41:31+01:00',
                    'image' => 'https://img.rawpixel.com/private/static/images/website/2022-05/fl12929410165-image-kybeoc61.jpg?w=800&dpr=1&fit=default&crop=default&q=65&vib=3&con=3&usm=15&bg=F4F4F3&ixlib=js-2.2.1&s=8d6e7f2dee775c830a74c4e0af9f0460',
                    'url' => 'https://www.courseticket.com/en/e/50'
                ],
                [
                    'id' => 2,
                    'notebook_id' => 2,
                    'title' => 'Note from notebook 2',
                    'date' => '2021-01-20T19:39:23+01:00',
                    'description' => 'Note test 2',
                    'completed' => '2021-01-25T16:39:23+01:00',
                    'position' => 1,
                    'created' => '2021-01-18T10:39:23+01:00',
                    'modified' => '2021-01-18T10:41:31+01:00',
                    'image' => null,
                    'url' => null
                ]
            ]
        ];
        $this->get($this->_getEndpoint().'1b6a83a918438b');
        $this->assertJsonResponseOK();
        $return = json_decode($this->_getBodyAsString(), true)['data'];
        $this->assertEquals($expectedData, $return);
    }
}
