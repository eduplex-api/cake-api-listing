<?php
declare(strict_types=1);

namespace Listing\Test\TestCase\Controller;

use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use Listing\ListingPlugin;
use Listing\Model\Table\NotesTable;
use Listing\Test\Fixture\NotebooksFixture;
use Listing\Test\Fixture\NotesFixture;
use RestApi\TestSuite\ApiCommonErrorsTest;


class NotesControllerTest extends ApiCommonErrorsTest
{
    public const NOTEBOOK_ID = 1;

    protected $fixtures = [
        NotesFixture::LOAD,
        NotebooksFixture::LOAD,
        UsersFixture::LOAD,
        OauthAccessTokensFixture::LOAD
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
    }

    protected function _getEndpoint(): string
    {
        return ListingPlugin::getRoutePath() . '/users/' . UsersFixture::BUYER_ID . '/notebooks/' . self::NOTEBOOK_ID. '/notes/';
    }

    public function testAddNew_Note()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'title' => 'Note 1',
            'description' => 'Note test 1',
            'date' => '2021-01-20 19:39:23',
            'completed' => '2021-01-25 16:39:23',
            'position' => 0
        ];

        $expected = [
            'id' => 6,
            'notebook_id' => self::NOTEBOOK_ID,
            'title' => 'Note 1',
            'date' => '2021-01-20T19:39:23+01:00',
            'description' => 'Note test 1',
            'completed' => '2021-01-25T16:39:23+01:00',
            'position' => 0,
            'image' => null,
            'url' => null
        ];
        $this->post($this->_getEndpoint(), $data);

        $this->assertJsonResponseOK();
        $return = json_decode($this->_getBodyAsString(), true)['data'];
        unset($return['created']);
        unset($return['modified']);
        $this->assertEquals($expected, $return);
    }

    public function testAddNew_WishListElement()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'title' => 'Whist list 2',
            'image' => 'https://img.rawpixel.com/private/static/images/website/2022-05/fl12929410165-image-kybeoc61.jpg?w=800&dpr=1&fit=default&crop=default&q=65&vib=3&con=3&usm=15&bg=F4F4F3&ixlib=js-2.2.1&s=8d6e7f2dee775c830a74c4e0af9f0460',
            'url' => 'https://www.courseticket.com/en/e/50',
            'position' => 0
        ];

        $expected = [
            'notebook_id' => self::NOTEBOOK_ID,
            'title' => 'Whist list 2',
            'date' => null,
            'description' => null,
            'completed' => null,
            'position' => 0,
            'image' => 'https://img.rawpixel.com/private/static/images/website/2022-05/fl12929410165-image-kybeoc61.jpg?w=800&dpr=1&fit=default&crop=default&q=65&vib=3&con=3&usm=15&bg=F4F4F3&ixlib=js-2.2.1&s=8d6e7f2dee775c830a74c4e0af9f0460',
            'url' => 'https://www.courseticket.com/en/e/50'
        ];

        $this->post($this->_getEndpoint(), $data);

        $this->assertJsonResponseOK();
        $return = json_decode($this->_getBodyAsString(), true)['data'];
        unset($return['created']);
        unset($return['modified']);
        $this->assertEqualsNoId($expected, $return);
    }

    public function testAddNew_toNotExistingUser()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $endpoint = ListingPlugin::getRoutePath() . '/users/300/notebooks/'. self::NOTEBOOK_ID .'/notes/';
        $data = [
            'title' => 'Note1 user 300',
            'description' => 'Note1 from the user 3',
            'date' => '2021-01-20 19:39:23',
            'completed' => '2021-01-25 16:39:23',
            'position' => 0
        ];

        $this->post($endpoint, $data);
        $this->assertException('Forbidden', 403, 'Resource not allowed with this token');
    }

    public function testAddNew_toNotExistingNotebook()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $endpoint = ListingPlugin::getRoutePath() . '/users/' . UsersFixture::BUYER_ID .'/notebooks/877/notes/';
        $data = [
            'title' => 'Note1 user 300',
            'description' => 'Note1 from the user 3',
            'date' => '2021-01-20 19:39:23',
            'completed' => '2021-01-25 16:39:23',
            'position' => 0
        ];

        $this->post($endpoint, $data);
        $this->assertResponseCode(403);
    }

    public function testGetData_ById()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $expectedData = [
            'id' => 1,
            'notebook_id' => self::NOTEBOOK_ID,
            'title' => 'Note 1',
            'date' => '2021-01-20T19:39:23+01:00',
            'description' => 'Note test 1',
            'completed' => '2021-01-25T16:39:23+01:00',
            'position' => 0,
            'created' => '2021-01-18T10:39:23+01:00',
            'modified' => '2021-01-18T10:41:31+01:00',
            'image' => null,
            'url' => null
        ];

        $this->get($this->_getEndpoint().'1');

        $this->assertJsonResponseOK();
        $return = json_decode($this->_getBodyAsString(), true)['data'];
        $this->assertEquals($expectedData, $return);
    }

    public function testGetData_NonExistingUser()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $endpoint = ListingPlugin::getRoutePath() . '/users/400/notebooks/'. self::NOTEBOOK_ID .'/notes/470';
        $this->get($endpoint);
        $this->assertResponseCode(403);
    }

    public function testGetData_NonExistingNotebook()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $endpoint = ListingPlugin::getRoutePath() . '/users/' . UsersFixture::BUYER_ID .'/notebooks/700/notes/470';
        $this->get($endpoint);
        $this->assertResponseCode(403);
    }

    public function testGetData_NonExistingNote()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $endpoint = ListingPlugin::getRoutePath() . '/users/' . UsersFixture::BUYER_ID .'/notebooks/'. self::NOTEBOOK_ID .'/notes/470';
        $this->get($endpoint);

        $this->assertException('Not Found', 404, 'Record not found in table');
    }

    public function testGetList_ofElementsFromNotebook()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $expectedData = [
            [
            'id' => 1,
            'notebook_id' => self::NOTEBOOK_ID,
            'title' => 'Note 1',
            'date' => '2021-01-20T19:39:23+01:00',
            'description' => 'Note test 1',
            'completed' => '2021-01-25T16:39:23+01:00',
            'position' => 0,
            'created' => '2021-01-18T10:39:23+01:00',
            'modified' => '2021-01-18T10:41:31+01:00',
            'image' => null,
            'url' => null
            ]
        ];

        $this->get($this->_getEndpoint());

        $return = $this->assertJsonResponseOK()['data'];
        $this->assertEquals($expectedData, $return);
    }

    public function testGetList_withNotExistingUser()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $endpoint = ListingPlugin::getRoutePath() . '/users/800/notebooks/'. self::NOTEBOOK_ID .'/notes/';
        $this->get($endpoint);
        $this->assertResponseCode(403);
    }

    public function testGetList_withNotExistingNotebook()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $endpoint = ListingPlugin::getRoutePath() . '/users/' . UsersFixture::BUYER_ID .'/notebooks/300/notes/';
        $this->get($endpoint);
        $this->assertResponseCode(403);
    }

    public function testEdit_noteChangingTitleAndDescription()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'title' => 'Note 1, Modificación',
            'description' => 'Modificando la nota'
        ];

        $this->patch($this->_getEndpoint() . '1', $data);
        $this->assertJsonResponseOK();
        $return = json_decode($this->_getBodyAsString(), true)['data'];
        $this->assertEquals($data['title'], $return['title']);
        $this->assertEquals($data['description'], $return['description']);
    }

    public function testEdit_editNoteChangePosition()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'position' => 5
        ];

        $this->patch($this->_getEndpoint() . '1', $data);
        $this->assertJsonResponseOK();
        $return = json_decode($this->_getBodyAsString(), true)['data'];
        $this->assertEquals($data['position'], $return['position']);
    }

    public function testEdit_editNonExistingUser()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $endpoint = ListingPlugin::getRoutePath() . '/users/600/notebooks/' . self::NOTEBOOK_ID. '/notes/1';
        $data = [
            'title' => 'Note 1, Modificación',
            'description' => 'Modificando la nota'
        ];
        $this->patch($endpoint, $data);
        $this->assertResponseCode(403);
    }

    public function testEdit_editNonExistingNotebook()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $endpoint = ListingPlugin::getRoutePath() . '/users/' . UsersFixture::BUYER_ID .'/notebooks/70/notes/1';
        $data = [
            'title' => 'Note 1, edit',
            'description' => 'Edit the note'
        ];
        $this->patch($endpoint, $data);
        $this->assertResponseCode(403);
    }

    public function testEdit_editNonExistingNote()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'title' => 'Note 1, Modificación',
            'description' => 'Modificando la nota'
        ];
        $this->patch($this->_getEndpoint().'900', $data);
        $this->assertResponseCode(404);
    }

    public function testDelete()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $noteId = 1;
        $this->delete($this->_getEndpoint() . $noteId);
        $this->assertResponseOk($this->_getBodyAsString());

        $note = NotesTable::load()->findNotesByNotebook($noteId)->first();

        $this->assertNull($note);
    }

    public function testDelete_withNotExistingUser()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $endpoint = ListingPlugin::getRoutePath() . '/users/565/notebooks/' . self::NOTEBOOK_ID. '/notes/1';
        $this->delete($endpoint);
        $this->assertResponseCode(403);
    }

    public function testDelete_withNotExistingNotebook()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $endpoint = ListingPlugin::getRoutePath() . '/users/' . UsersFixture::BUYER_ID .'/notebooks/700/notes/1';
        $this->delete($endpoint);
        $this->assertResponseCode(403);
    }

    public function testDelete_withNotExistingNote()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $noteId = 15;
        $this->delete($this->_getEndpoint() . $noteId);
        $this->assertResponseError($this->_getBodyAsString());
    }
}
